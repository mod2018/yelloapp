import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { StyleSheet, Text, View, Image, ImageBackground, TextInput } from 'react-native';


import { GreenButton } from '../components/Button';

export default class EnterRoute extends React.Component {

  static navigationOptions = {
    title: 'Select School',
  };

  render() {
    let data = [{
        value: 'Banana',
      }, {
        value: 'Mango',
      }, {
        value: 'Pear',
      }];

    return (
      <ImageBackground source={require('../assets/imageBackground.png')} style={{flex: 1}}>
        <View style={styles.container}>
          <View>
            <View>

              <Text style={styles.mainTagText}>
              Select your school from the list below
              </Text>

              <View style={{backgroundColor:'#fff', paddingLeft: 5, paddingRight: 5, borderRadius: 5}}>
              <Dropdown
              label = 'Please Select A School'
              data = {data}
              baseColor = '#000'
              textColor = '#000'
                     />
              </View>

              <GreenButton
              onPress={() =>
                this.props.navigation.navigate('SchoolRoutes')
              }
              text="Continue" />
            </View>
          </View>
          <View style={{alignItems: 'center', marginTop: 25}}>
          <View>
            <Text style={{color: '#fff'}}>
              Looking to setup Yello in your district?
            </Text>
          </View>
          <View>
            <Text style={{color: '#fff'}}>
              Contact us to setup a free demo!
            </Text>
          </View>

        </View>
        </View>

      </ImageBackground>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  },
  mainTagText: {
    fontSize: 38,
    color: '#fff',
    textAlign: 'center',
    paddingTop: 50,
    paddingBottom: 50

  }
});
