import React from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { StyleSheet, Text, ScrollView, Image, ImageBackground, TextInput } from 'react-native';

import { SchoolRoute } from '../components/SchoolRoute';

export default class SchoolRoutes extends React.Component {
  static navigationOptions = {
    title: 'St. Mary\'s Oakville',
  };

  render() {
    return (
      <ImageBackground source={require('../assets/imageBackground.png')} style={{flex: 1}}>
        <ScrollView style={styles.container}>
          <SchoolRoute
          text='School Route 1'
          active='yes'
          />
        </ScrollView>
      </ImageBackground>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 5,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  },
  mainTagText: {
    fontSize: 38,
    color: '#fff',
    textAlign: 'center',
    paddingTop: 50,
    paddingBottom: 50

  }
});
