import React from 'react';
import { AsyncStorage, StyleSheet, Text, View, Image, ImageBackground, TextInput } from 'react-native';

import { GreenButton } from '../components/Button';

export default class EnterRoute extends React.Component {

  constructor(props) {
      super(props);
      this.state = { text: 'Bus Route Code' };
    }

    clearPlaceholder() {
    this.setState({
      text: ''
    });
  }

    routeExists(responseJson, code){
      if(responseJson.data != null && code != "") {
        this.props.navigation.navigate('RouteConfirmation' , {routeCode: code});
      } else{
        alert('Please enter a valid bus route code');
      }
    }

   routeCheck(code){
    let url = 'http://192.168.0.2:8080/api/routes/';
    url = url + encodeURI(code);
    return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => response = this.routeExists(responseJson, code))
      .catch((error) => {
        alert('Error Connecting To API');
        console.error(error);
      });
  }

  static navigationOptions = {
    title: 'Enter Route',
  };

  render() {



    return (
      <ImageBackground source={require('../assets/imageBackground.png')} style={{flex: 1}}>
        <View style={styles.container}>
          <View style={{}}>
            <View>
              <Text style={{color: 'grey', marginBottom: 10, fontSize: 16}}>Enter Bus Route Code</Text>
              <TextInput
                style={{height: 50, color: '#fff', fontSize: 24, borderColor: '#fff', borderWidth: 1, borderRadius: 5}}
                onFocus={() => this.clearPlaceholder()}
                onChangeText={(text) => this.setState({text})}
                value={this.state.text}
              />
              <Text style={{color: '#fff', marginTop: 10, fontSize: 16}}>Codes are not case sensitive</Text>
            </View>

            <View>
              <GreenButton
                onPress={() =>
                this.routeCheck(this.state.text)
                }
                text="Start Route"
              />
            </View>
          </View>
          </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: 10,
    marginTop: -20,
    paddingHorizontal: 20,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  }
});
