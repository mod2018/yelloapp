import React from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, TextInput } from 'react-native';
import { Permissions } from 'expo';


import { RedButton } from '../components/Button';


export default class EnterRoute extends React.Component {

  state = {
    errorMessage: null
  };

  static navigationOptions = {
    title: 'Route Confirmation',
    gesturesEnabled: false,
    headerLeft: null
  };

  //Update route to active and update with current geo coordinates.
  componentWillMount(){
    let url = 'http://192.168.0.2:8080/api/routes/';
    url = url + encodeURI(this.props.navigation.getParam('routeCode', 'NO-ID'));
    fetch(url, {
      method: 'PATCH',
      headers: {
        dataType: 'json',
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        active: 'true',
        latitude: '8888887',
        longitude: '72376762'
      }),
    });
  }




  endRoute(){
    let url = 'http://192.168.0.2:8080/api/routes/';
    url = url + encodeURI(this.props.navigation.getParam('routeCode', 'NO-ID'));
    fetch(url, {
      method: 'PATCH',
      headers: {
        dataType: 'json',
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        active: 'false',
        latitude: '8888887',
        longitude: '72376762'
      }),
    });
    this.props.navigation.navigate('EnterRoute')
  }

  render() {


  //alert(this.state.location.coords["latitude"]);
    let routeCode = this.props.navigation.getParam('routeCode', 'NO-ID');
    routeCode = 'You Are Currently Driving Route #'+ routeCode;


    return (
      <ImageBackground source={require('../assets/imageBackground.png')} style={{flex: 1}}>
        <View style={styles.container}>
          <View>
            <View>
            <View style={{alignItems: 'center'}}>
              <Image
                source={require('../assets/checkMark.png')}
                style={styles.checkmark}/>
                </View>

              <Text style={styles.mainTagText}>
              {routeCode}
              </Text>

              <RedButton
              onPress={() => this.endRoute()}
              text="End Route" />
            </View>
          </View>
        </View>

      </ImageBackground>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: 10,
    marginTop: -20,
    paddingHorizontal: 20,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  },
  mainTagText: {
    fontSize: 38,
    color: '#fff',
    textAlign: 'center',
    padding: 20,

  },
  checkmark: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    marginTop: 40
  },
});
