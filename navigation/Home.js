import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';

import { Button } from '../components/Button';

export default class Home extends React.Component {
  
  static navigationOptions = ({ navigation }) => {
    return {
       header: () => null
    }
}
  render() {
    return (
      <ImageBackground source={require('../assets/imageBackground.png')} style={{flex: 1}}>
      <View style={styles.container}>
          <View style={styles.welcomeImageContainer}>
            <Image
              source={require('../assets/mainIcon.png')}
              style={styles.welcomeImage}/>
          </View>

          <View>
            <Text style={styles.mainTagText}>
            Stop wondering where your bus is.
            </Text>
          </View>


          <View style={styles.buttonContainer}>
            <View>
              <Button
              onPress={() =>
                this.props.navigation.navigate('EnterRoute')
              }
          text="I'm a Driver" />
            </View>

            <View>
              <Button onPress={() =>
                this.props.navigation.navigate('SchoolSelect')
              }
              text="I'm a Passenger" />
            </View>
          </View>
      </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  },
  welcomeImageContainer: {
    alignItems: 'center',
    marginTop: 40
  },
  welcomeImage: {
    width: 350,
    height: 100,
    resizeMode: 'contain',
    marginTop: 30,
    marginLeft: -10,
  },
  mainTagText: {
    fontSize: 38,
    color: '#fff',
    marginTop: 60,
    textAlign: 'center',
    padding: 20,
  },
  buttonContainer: {
    marginTop: 50
  }

});
