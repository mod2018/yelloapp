  import { createStackNavigator, createAppContainer } from 'react-navigation';

  import Home from './Home';

  import EnterRoute from './EnterRoute';
  import RouteConfirmation from './RouteConfirmation';

  import SchoolSelect from './SchoolSelect';
  import SchoolRoutes from './SchoolRoutes';

const AppNavigator = createStackNavigator({
  Home: { screen: Home},
  EnterRoute: { screen: EnterRoute },
  RouteConfirmation: { screen: RouteConfirmation },
  SchoolSelect: { screen: SchoolSelect },
  SchoolRoutes: { screen: SchoolRoutes },
  },{
    defaultNavigationOptions: {
      title: 'Current Page Title',
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: '#0B2D49',
        marginTop: 10,
        marginBottom: 10,
        borderBottomColor: '#0B2D49'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 24
      }}});

const Navigation = createAppContainer(AppNavigator);

export default Navigation;
