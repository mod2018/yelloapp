import React from 'react';
import { Text, TouchableOpacity } from 'react-native';


const SchoolRoute = ({ text, active, onPress }) => {
  const { buttonStyle, textStyle } = styles;

  return(
    <TouchableOpacity style={buttonStyle}>
      <Text style={textStyle}>
        {text}
      </Text>
      <Text>
      {active}
      </Text>
    </TouchableOpacity>
  );
};


const styles = {
  textStyle:{
    color: '#000',
    fontSize: 21,
    fontWeight: 'bold',


  },
  buttonStyle:{
    backgroundColor: '#fff',
    fontWeight: 'bold',
    textAlign:'center',
    padding: 20,
    paddingLeft: 8,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  }
};

export {SchoolRoute};
