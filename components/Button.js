import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ text, onPress }) => {
  const { buttonStyle, textStyle } = styles;

  return(
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const RedButton = ({ text, onPress }) => {
  const { redButtonStyle, whiteTextStyle } = styles;

  return(
    <TouchableOpacity onPress={onPress} style={redButtonStyle}>
      <Text style={whiteTextStyle}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const GreenButton = ({ text, onPress }) => {
  const { greenButtonStyle, whiteTextStyle } = styles;

  return(
    <TouchableOpacity onPress={onPress} style={greenButtonStyle}>
      <Text style={whiteTextStyle}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle:{
    color: '#000',
    fontSize: 21,
    fontWeight: 'bold',
    textAlign:'center',
  },
  whiteTextStyle:{
    color: '#fff',
    fontSize: 21,
    fontWeight: 'bold',
    textAlign:'center',
  },
  buttonStyle: {
    backgroundColor: '#FFDE23',
    borderRadius: 5,
    overflow: 'hidden',
    padding: 18,
    marginTop: 15
  },
  redButtonStyle: {
    backgroundColor: '#E6173E',
    borderRadius: 5,
    overflow: 'hidden',
    padding: 18,
    marginTop: 15
  },
  greenButtonStyle: {
    backgroundColor: '#44A45F',
    borderRadius: 5,
    overflow: 'hidden',
    padding: 18,
    marginTop: 15
  }
};

export { Button, RedButton, GreenButton };
