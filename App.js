import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';

import Navigation from './navigation/AppNavigator';
import Button from './components/Button';

export default class App extends React.Component {
   
  render() {
    return (
        <Navigation style={{flex: 1}} />

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
    borderBottomColor: '#FFDE23',
    borderBottomWidth: 20
  },
  welcomeImageContainer: {
    alignItems: 'center',
    marginTop: 40
  },
  welcomeImage: {
    width: 350,
    height: 100,
    resizeMode: 'contain',
    marginTop: 30,
    marginLeft: -10,
  },
  mainTagText: {
    fontSize: 38,
    color: '#fff',
    marginTop: 60,
    textAlign: 'center',
    padding: 20,
  },
  buttonContainer: {
    marginTop: 50
  }

});
